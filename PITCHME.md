# **Comm' skills** 

*By `Leon`*

---

## 1. Goal

> Do not lose time and succeed a presentation
> * Why 
> * Planning  
> ...

![](assets/img/goal.png)

---?color=linear-gradient(180deg, black 10%, white 90%)
@snap[south span-500]
## Focus

> Warn about contenance *no hiding...*  

> Be sure, no hestiation  
    
> Define structure:  
    >* body
    >* conclusion
    >* ...

> Deliver the message (keep focus)
@snapend

---

## 3. Tips

> Use stories:
>* Pursuit of a goal
>* "*`let me tell you a story`*"...

---

## 4. To avoid

> Fat fingering

> Loosing audience

> Training <> Demonstrating

> Talking features and not stories

---

## 5. Deal with questions